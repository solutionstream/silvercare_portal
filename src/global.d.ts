import "bootstrap";
import * as _Axios from "axios";
import * as _Vue from "vue";
import _Navigo = require("navigo");

declare module "axios" {
    interface AxiosRequestConfig {
        token?: string;
    }
}

declare global {

    export type VueType = typeof _Vue;

    export interface ComponentOptionsPlus<V extends _Vue> extends _Vue.ComponentOptions<V> {
        $data?: object;
    }

    export interface VueStatic<T extends _Vue> extends VueType {
        new(): T;
        extend(options: ComponentOptionsPlus<T>): VueType & T;
    }

    export interface MiniToastr {
        init(config: { timeout: number }): void;
        success(message: string, title?: string, timeout?: number): void;
        info(message: string, title?: string, timeout?: number): void;
        warn(message: string, title?: string, timeout?: number): void;
        error(message: string, title?: string, timeout?: number): void;
        setIcon(type: string, elem: string, attrs: { [key: string]: string }): void;
    }

    export type Vue = _Vue;
    export const Vue: typeof _Vue
    export const Navigo: typeof _Navigo;
    export const ObjectID: any;
    export const axios: _Axios.AxiosStatic;
    export const miniToastr: MiniToastr;
    export namespace Axios {
        export type Transformer = _Axios.AxiosTransformer;
        export type Adapter = _Axios.AxiosAdapter;
        export type BasicCredentials = _Axios.AxiosBasicCredentials;
        export type ProxyConfig = _Axios.AxiosProxyConfig;
        export type RequestConfig = _Axios.AxiosRequestConfig;
        export type Response = _Axios.AxiosResponse;
        export type Error = _Axios.AxiosError;
        export type Promise = _Axios.AxiosPromise;
        export type CancelStatic = _Axios.CancelStatic;
        export type Cancel = _Axios.Cancel;
        export type Canceler = _Axios.Canceler;
        export type CancelTokenStatic = _Axios.CancelTokenStatic;
        export type CancelToken = _Axios.CancelToken;
        export type CancelTokenSource = _Axios.CancelTokenSource;
    }
}
