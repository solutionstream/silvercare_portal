// Create a new BSON ObjectID
function id(): string {
    return new ObjectID().toString();
}

// Simple querystring parser
function qs(query?: string) {
    const dict: { [key: string]: string; } = {};
    if (!query) { return dict; }

    const pairs = query.split("&");
    if (pairs.length === 0) { return dict; }

    for (const pair of pairs) {
        const [key, value] = pair.split("=", 2);
        dict[key] = (typeof value === "undefined")
            ? ""
            : decodeURIComponent(value.replace(/\+/g, " "));
    }

    return dict;
}

// Naive querystring parser
const query = qs(location.href.split("?")[1]);


// https://stackoverflow.com/a/14919494
function humanize(bytes: number, si: boolean) {
    const thresh = si ? 1000 : 1024;
    if (Math.abs(bytes) < thresh) {
        return bytes + " B";
    }
    const units = si
        ? ["KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
        : ["KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"];
    let u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while (Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1) + " " + units[u];
}

// Draws an image onto a canvas element
function imageCanvas(img: HTMLImageElement) {
    // Create a canvas element to render our image in
    const canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    // Render the image in a 2d canvas
    const context = canvas.getContext("2d");
    if (!context) { throw new Error("Failed to get canvas context"); }

    // Draw image on canvas
    context.drawImage(img, 0, 0);
    return canvas;
}

// Draws an image onto a canvas from a URL
async function urlCanvas(url: string) {
    // Create an anonymous image element to load our image with
    const image = new Image();
    image.setAttribute("crossOrigin", "anonymous");

    return new Promise<HTMLCanvasElement>((resolve, reject) => {
        // Propogate error when loading image fails
        image.addEventListener("error", (event) => reject(event.error));

        // Wait for the image to be downloaded
        image.addEventListener("load", () => {
            try {
                resolve(imageCanvas(image));
            } catch (error) {
                reject(error);
            }
        });

        // Initiate download of the image
        image.src = url;
    });
}

// Get the data url for an image element
async function getCanvasURL(canvas: HTMLCanvasElement) {
    return canvas.toDataURL("image/png");
}

// Get a data blob for a given canvas
async function getCanvasData(canvas: HTMLCanvasElement) {
    return new Promise<null|Blob>((resolve) => {
        canvas.toBlob(resolve, "image/png");
    });
}

namespace debounce {
    export interface Function<T> { (...args: any[]): T | Promise<T>; }
    export interface Result<T> { (...args: any[]): Promise<T>; }
}

// Delay calling a function until a timeout has elapsed
function debounce<T>(fn: debounce.Function<T>, wait: number): debounce.Result<T> {
    let timeout: null | number = null;

    // Return a function that only calls the original after some time as elapsed
    // Calling the function again will restart the timer
    return function(this: any, ...args: any[]): Promise<T> {
        if (timeout) { clearTimeout(timeout); }

        return new Promise<T>((resolve, reject) => {
            timeout = setTimeout(() => {
                timeout = null;
                try {
                    const result = fn.apply(this, args);
                    resolve(result);
                } catch (error) {
                    reject(error);
                }
            }, wait);
        });
    };
}

// Remove all instances of element from collection
function pull<T>(collection: T[], remove: T, eq?: (t1: T, t2: T) => boolean): T[] {
    return collection.filter(
        (elem) => eq ? !eq(elem, remove) : elem !== remove);
}

// Remove all elements in collection 2 from collection 1
function pullAll<T>(collection: T[], remove: T[], eq?: (t1: T, t2: T) => boolean): T[] {
    return remove.reduce(
        (collection, remove) => pull(collection, remove, eq), collection);
}

function exhaustiveCheck(value: never): any {
    throw new Error(`Unexpected value: ${value}`);
}

interface Token {
    header: Dictionary<any>;
    body: Dictionary<any>;
    signature: string;
}

function parse_jwt(token: string): undefined|Token {
    if (!token) { return undefined; }

    try {
        const [b64_header, b64_body, signature] = token.split(".");
        const header = JSON.parse(atob(b64_header));
        const body = JSON.parse(atob(b64_body));
        return { header, body, signature };
    } catch (error) {
        console.error(error);
        return undefined;
    }
}

type ID = string;
type Color = string;
interface Address { type: string; address: string; }
interface Phone { type: string; number: string; }
interface Email { type: string; email: string; }

// Dictionary of type T
interface Dictionary<T> { [key: string]: T; }

// Media file
interface Media {
    id: ID;
    mimetype: string;
    size: number;
    filename: string;
    tags: string[];
    created: string;
    updated: string;
    data: null | Blob;
}

namespace Media {
    export type Status = "new" | "pending" | "error" | "complete";
    export interface Attachment {
        user: ID;
        file: ID;
    }

    export interface Fat extends Media {
        status: Status;
    }
}

// A User object
interface User {
    id: ID;
    username: string;
    password?: string;
    roles: string[];
}

namespace User {
    export type Ref = Pick<User, "id"|"username">;
    export interface Info {
        id: ID;
        username: string;
        roles: string[];
        patient_id?: ID;
        caregiver_id?: ID;
        pac_id?: ID;
    }
}

interface CareGiver {
    id: ID;
    full_name: string;
    invites: CareGiver.Invite[];
    address: undefined | string;
    phone: undefined | string;
    email: undefined | string;
}

namespace CareGiver {
    export interface Invite {
        id: ID;
        full_name: string;
        relationship: string;
        created: string;
    }
}

interface PAC {
    id: ID;
    full_name: string;
    theme: PAC.Theme;
    address: undefined | string;
    phone: undefined | string;
    email: undefined | string;
}

namespace PAC {
    export type Ref = Pick<PAC, "id"|"full_name">;
    export interface Theme {
        icon: null | Media.Attachment;
        primary_color: Color;
    }
}

interface Patient {
    id: string;
    user_id: string;
    pac_id: string;
    full_name: string;
    preferred_name: string;
    date_of_birth: string;
    gender: string;
    blood_type: string;
    faith: string;
    instructions: string;
    conditions: string[];
    allergies: string[];
    address: undefined | string;
    phone: undefined | string;
    email: undefined | string;
}

interface Version {
    major: number;
    minor: number;
    patch: number;
}

namespace Version {
    export function is(version: any) {
        return typeof version === "object"
            && typeof version.major === "number"
            && typeof version.minor === "number"
            && typeof version.patch === "number";
    }

    export function stringify(version: Version): string {
        if (!Version.is(version)) { return ""; }
        return `${version.major}.${version.minor}.${version.patch}`;
    }

    export function parse(version: string): Version {
        const [major, minor, patch] = version.split(".");
        return {
            major: parseInt(major, 10),
            minor: parseInt(minor, 10),
            patch: parseInt(patch, 10),
        };
    }
}

namespace Phone {
    export function parse(number: string): string {
        let digits = number.replace(/[^0-9]/g, "");
        if (digits.substr(0, 1) !== "1") {
            digits = `1${digits}`;
        }
        return `+${digits}`;
    }

    export function format(number: string): string {
        const phone = Phone.parse(number);
        return phone.replace(/\+1(\d{3})(\d{3})(\d{3})/, "($1) $2-$3");
    }
}

namespace Device {

    const sms = "sms";
    const email = "email";
    const pers = "pers";
    const ios = "ios";
    const android = "android";

    export const Type = { ios, android, sms, email, pers };
    export type Type = keyof typeof Type;

    export interface Normal {
        id: ID;
        type: string;
        created: string;
        value: any;
    }

    // Device definition for mobile devices
    export interface Mobile {
        id: ID;
        type: "ios" | "android";
        created: string;
        version: { major: number, minor: number, patch: number };
    }

    // Device definition for SMS only devices
    export interface SMS {
        id: ID;
        type: "sms";
        created: string;
        phone_number: string;
    }

    // Device definition for Email only devices
    export interface Email {
        id: ID;
        type: "email";
        created: string;
        email_address: string;
    }

    // Device definition for PERS devices
    export interface PERS {
        id: ID;
        type: "pers";
        created: string;
        identifier: string;
    }

    // Normalize any device
    export function normalize(device: Device): Normal {
        const normal = {
            id: device.id,
            type: device.type,
            created: device.created,
        };

        switch (device.type) {
            case "ios":
            case "android":
                return { ...normal, value: device.version };
            case "email":
                return { ...normal, value: device.email_address };
            case "sms":
                return { ...normal, value: device.phone_number };
            case "pers":
                return { ...normal, value: device.identifier };
            default: return exhaustiveCheck(device);
        }
    }

    // Denormalize any device
    export function denormalize(normal: Normal): Device {
        const device = {
            id: normal.id,
            type: normal.type as Device.Type,
            created: normal.created,
        } as Device;

        switch (device.type) {
            case "ios":
            case "android":
                device.version = Version.parse(normal.value);
                break;
            case "sms":
                device.phone_number = normal.value;
                break;
            case "email":
                device.email_address = normal.value;
                break;
            case "pers":
                device.identifier = normal.value;
                break;
            default: return exhaustiveCheck(device);
        }

        return device;
    }

    export function from_server(server: { type: Device.Type, [key: string]: any }): Device {
        const device = {
            id: browser.string(server._id),
            type: browser.string(server.type),
            created: browser.date(server.created),
        } as Device;

        switch (device.type) {
            case "ios":
            case "android":
                device.version = server.version;
                break;
            case "sms":
                device.phone_number = server.phone_number;
                break;
            case "email":
                device.email_address = server.email_address;
                break;
            case "pers":
                device.identifier = server.identifier;
                break;
            default: return exhaustiveCheck(device);
        }

        return device;
    }
}

type Device = Device.Mobile | Device.SMS | Device.Email | Device.PERS;


// Create a pac object from partial values
function make_pac(options: Partial<PAC>) {
    const pac: PAC = {
        id: options.id || "",
        full_name: options.full_name || "",
        address: options.address,
        phone: options.phone,
        email: options.email,
        theme: {
            icon: options.theme ? options.theme.icon : null,
            primary_color: options.theme ? options.theme.primary_color : "",
        },
    };

    return pac;
}

// Create a patient object from partial values
function make_patient(options: Partial<Patient>) {
    const patient: Patient = {
        id: options.id || "",
        user_id: options.user_id || "",
        pac_id: options.pac_id || "",
        full_name: options.full_name || "",
        preferred_name: options.preferred_name || "",
        date_of_birth: options.date_of_birth || "",
        gender: options.gender || "",
        blood_type: options.blood_type || "",
        faith: options.faith || "",
        instructions: options.instructions || "",
        conditions: options.conditions || [],
        allergies: options.allergies || [],
        address: options.address || "",
        phone: options.phone || "",
        email: options.email || "",
    };

    return patient;
}

// Create a user object from partial values
function make_user(options: Partial<User>) {
    const user: User = {
        id: options.id || "",
        username: options.username || "",
        password: options.password,
        roles: options.roles || [],
    };

    return user;
}

// Create a user info object from partial values
function make_user_info(options: Partial<User.Info>) {
    const info: User.Info = {
        id: options.id || "",
        username: options.username || "",
        roles: options.roles || [],
        pac_id: options.pac_id || "",
        patient_id: options.patient_id || "",
        caregiver_id: options.caregiver_id || "",
    };

    return info;
}

// Create a caregiver object from partial values
function make_caregiver(options: Partial<CareGiver>) {
    const caregiver: CareGiver = {
        id: options.id || "",
        full_name: options.full_name || "",
        invites: options.invites || [],
        address: options.address,
        phone: options.phone,
        email: options.email,
    };

    return caregiver;
}

// Create media object from partial values
function make_media(options: Partial<Media>) {
    const photo: Media = {
        id: options.id || "",
        data: options.data || null,
        created: options.created || "",
        updated: options.updated || "",
        mimetype: options.mimetype || "",
        size: options.size || 0,
        filename: options.filename || "",
        tags: options.tags || [],
    };

    return photo;
}

// Create media object from partial values
function make_fat_media(options: Partial<Media.Fat> & Pick<Media.Fat, "status">) {
    const photo: Media.Fat = {
        ...make_media(options),
        status: options.status,
    };

    return photo;
}

namespace State {
    export interface API {
        base_url: string;
    }

    export interface User {
        id: string;
        username: string;
        password: string;
        roles: string[];
        caregiver_id: undefined | string;
        pac_id: undefined | string;
        patient_id: undefined | string;
    }
}

interface State {
    route: undefined | typeof Vue;
    api: State.API;
    user: State.User;
    pac: PAC.Ref;
}

const state: State = {
    route: undefined,
    api: { base_url: "http://localhost:8000" },
    user: {
        id: "",
        username: "",
        password: "",
        roles: [],
        pac_id: "",
        patient_id: "",
        caregiver_id: "",
    },
    pac: {
        id: "",
        full_name: "",
    },
};

type _PACREF = PAC.Ref;

namespace State {
    export namespace PAC {
        export function load(pac: _PACREF) {
            state.pac = pac;
        }
    }

    export namespace User {

        // Log a user in
        export function login(user: State.User) {
            state.user = user;
        }

        // Log a user out
        export function logout() {
            state.pac = make_pac({});
            state.user = {
                id: "",
                username: "",
                password: "",
                roles: [],
                pac_id: "",
                patient_id: "",
                caregiver_id: "",
            };
        }

        export function has_role(...roles: string[]) {
            if (!roles.length) { return true; }
            return !!roles.find(
                (role) => state.user.roles.indexOf(role) >= 0);
        }

        export function is_caregiver() {
            return has_role("caregiver");
        }

        export function is_pac_admin() {
            return has_role("pac_admin");
        }

        export function is_sv_admin() {
            return has_role("sv_admin") || has_role("sv_super");
        }

        export function is_sv_super() {
            return has_role("sv_super");
        }
    }
}

class APIError extends Error {
    constructor(
        public config: Axios.RequestConfig,
        public code?: string,
        public response?: Axios.Response) {
        super(code || "APIError");
    }
}

// Initialize our toaster
const toast = miniToastr;
toast.init({ timeout: 10000 });
toast.setIcon("error", "i", { "class": "fa fa-warning" });
toast.setIcon("warn", "i", { "class": "fa fa-exclamation-triangle" });
toast.setIcon("info", "i", { "class": "fa fa-info-circle" });
toast.setIcon("success", "i", { "class": "fa fa-check-circle-o" });

interface MiniToastr {
    api_error(message: string, title?: string, error?: any): void;
}

// Utility for showing API debug messages
toast.api_error = (message: string, title: string = "Error!", error?: any) => {
    toast.error(message, title);
    if (!error) { return; }

    console.error(error.response || error);
    const { response: { data: {debug} }} = error;
    if (!debug) { return; }

    toast.warn(debug, "Debug!");
};

// Create an API instance
const api = axios.create({
    baseURL: state.api.base_url,
});

// Intercept requests to ensure that the user is authenticated
async function enforce_authentication(config: Axios.RequestConfig) {
    const { username, password } = state.user;

    // Authenticate the user with a token
    if (config.token) {
        config.headers = config.headers || {};
        config.headers.Authorization = `Bearer ${config.token}`;
        delete config.auth;
    }

    // Add current user credentials to the request (if none exist)
    else if (typeof config.auth === "undefined" && username && password) {
        config.auth = { username, password };
    }

    return config;
}

// Intercept request to clear data when user is unauthenticated
async function enforce_authentication_res(error: APIError) {
    if (error.response) {
        // Handle messages from the server to authenticate
        if (error.response.status === 401 || error.response.headers["www-authenticate"]) {
            // router.navigate("/logout");
        } else if (error.response.status === 404) {
            return undefined; // Return nothing when resource not found
        }
    }

    throw error;
}

api.interceptors.request.use(enforce_authentication);
api.interceptors.response.use((res) => res, enforce_authentication_res);


namespace server {
    export function string(str?: string): undefined|string {
        if (!str) { return undefined; }
        return str;
    }

    export function date(date?: string): undefined|number {
        if (!date) { return undefined; }
        return new Date(date).getTime();
    }

    export function address(address?: string): undefined|Address {
        if (!address) { return undefined; }
        return { type: "home", address };
    }

    export function phone(number?: string): undefined|Phone {
        if (!number) { return undefined; }
        const phone = Phone.parse(number);
        return { type: "home", number: phone };
    }

    export function email(email?: string): undefined|Email {
        if (!email) { return undefined; }
        return { type: "home", email };
    }
}

namespace browser {
    export function string(str: string) {
        return str || "";
    }

    export function number(num: string|number) {
        return parseInt(num as string, 10);
    }

    export function date(date: number) {
        if (!date) { return ""; }

        const dt = new Date(date);
        const yyyy = dt.getFullYear();
        const MM = ("0" + (dt.getMonth() + 1)).slice(-2);
        const dd = ("0" + dt.getDate()).slice(-2);

        return `${yyyy}-${MM}-${dd}`;
    }

    export function address(address?: Address): string {
        if (!address) { return ""; }
        return address.address;
    }

    export function phone(phone?: Phone): string {
        if (!phone) { return ""; }
        return phone.number;
    }

    export function email(email?: Email): string {
        if (!email) { return ""; }
        return email.email;
    }
}

async function search_users(username: string, token?: Axios.CancelToken) {
    const response = await api.get(`/user/?limit=10&username=${username}`, {
        cancelToken: token,
    });

    // Create user objects from the results
    const users: any[] = response.data.users;
    const matches: User[] = users.map(
        ({ _id: id, username, roles }) => ({ id, username, roles }));

    return matches;
}

async function fetch_user_info(user_id?: ID, token?: Axios.CancelToken) {
    if (!user_id) { return undefined; }

    const user_info = await api.get(`/user/${user_id}/info`, {
        cancelToken: token,
    });

    if (!user_info || !user_info.data) { return undefined; }

    const data = user_info.data;
    const info: User.Info = {
        id: browser.string(data._id),
        username: browser.string(data.username),
        roles: data.roles.map(browser.string),
        patient_id: browser.string(data.patient_id),
        caregiver_id: browser.string(data.caregiver_id),
        pac_id: browser.string(data.pac_id),
    };

    return info;
}

async function fetch_patient(patient_id?: ID, token?: Axios.CancelToken) {
    if (!patient_id) { return undefined; }

    const response = await api.get(`/patient/${patient_id}`, {
        cancelToken: token,
    });

    if (!response || !response.data) { return undefined; }

    // Cleanup data from server
    const data = response.data;
    const patient: Patient = {
        id: browser.string(data._id),
        user_id: browser.string(data.user_id),
        pac_id: browser.string(data.pac_id),
        full_name: browser.string(data.full_name),
        preferred_name: browser.string(data.preferred_name),
        date_of_birth: browser.date(data.date_of_birth),
        gender: browser.string(data.gender),
        blood_type: browser.string(data.blood_type),
        faith: browser.string(data.faith),
        instructions: browser.string(data.instructions),
        conditions: data.conditions.map(browser.string),
        allergies: data.allergies.map(browser.string),
        address: browser.address(data.addresses[0]),
        phone: browser.phone(data.phones[0]),
        email: browser.email(data.emails[0]),
    };

    return patient;
}

async function save_patient(user: User.Ref, patient: Patient, token?: Axios.CancelToken) {
    const put_url = patient.id
        ? `/patient/${patient.id}`
        : `/user/${user.id}/patient/${id()}`;

    const response = await api.put(put_url, {
        pac_id: server.string(patient.pac_id),
        full_name: server.string(patient.full_name),
        preferred_name: server.string(patient.preferred_name),
        date_of_birth: server.date(patient.date_of_birth),
        gender: server.string(patient.gender),
        blood_type: server.string(patient.blood_type),
        faith: server.string(patient.faith),
        instructions: server.string(patient.instructions),
        conditions: patient.conditions,
        allergies: patient.allergies,
        address: server.address(patient.address),
        phone: server.phone(patient.phone),
        email: server.email(patient.email),
    }, { cancelToken: token });

    const data = response.data;
    const result: Patient = {
        id: browser.string(data._id),
        user_id: browser.string(data.user_id),
        pac_id: browser.string(data.pac_id),
        full_name: browser.string(data.full_name),
        preferred_name: browser.string(data.preferred_name),
        date_of_birth: browser.date(data.date_of_birth),
        gender: browser.string(data.gender),
        blood_type: browser.string(data.blood_type),
        faith: browser.string(data.faith),
        instructions: browser.string(data.instructions),
        conditions: data.conditions.map(browser.string),
        allergies: data.allergies.map(browser.string),
        address: browser.address(data.addresses[0]),
        phone: browser.phone(data.phones[0]),
        email: browser.email(data.emails[0]),
    };

    return result;
}

async function fetch_caregiver(caregiver_id?: ID, token?: Axios.CancelToken) {
    if (!caregiver_id) { return undefined; }

    const response = await api.get(`/caregiver/${caregiver_id}`, {
        cancelToken: token,
    });

    if (!response || !response.data) { return undefined; }

    // Cleanup data from server
    const data = response.data.caregiver;
    const caregiver: CareGiver = {
        id: browser.string(data._id),
        full_name: browser.string(data.full_name),
        address: browser.address(data.addresses[0]),
        phone: browser.phone(data.phones[0]),
        email: browser.email(data.emails[0]),
        invites: data.invites.map((invite: any) => ({
            id: browser.string(invite._id),
            full_name: browser.string(invite.full_name),
            relationship: browser.string(invite.relationship),
            created: browser.date(invite.created),
        })),
    };

    return caregiver;
}

async function save_user(user: User, token?: Axios.CancelToken) {
    const { username, password, roles } = user;

    // Create or update the user
    const response = await api.request({
        url: user.id ? `/user/${user.id}` : `/user/${id()}`,
        method: user.id ? "POST" : "PUT",
        data: { username, password, roles },
        cancelToken: token,
    });

    // Unpack the resulting user object
    const result: User = {
        id: response.data._id,
        username: response.data.username,
        roles: response.data.roles,
    };

    return result;
}

function media_form(fields: Dictionary<string>, data: Blob): FormData {
    const form_data = new FormData();

    for (const key in fields) {
        if (fields.hasOwnProperty(key)) {
            form_data.set(key, fields[key]);
        }
    }

    form_data.set("file", data);
    return form_data;
}

function to_media(partial: Partial<Media>) {
    const media: Media = {
        id: partial.id || "",
        filename: partial.filename || "",
        tags: partial.tags || [],
        created: partial.created || "",
        updated: partial.updated || "",
        size: partial.size || 0,
        mimetype: partial.mimetype || "",
        data: partial.data || null,
    };

    return media;
}

function file_to_media(file?: File) {
    const media: Media = to_media({
        data: file || null,
        size: file && file.size,
        filename: file && file.name,
        mimetype: file && file.type,
    });

    return media;
}

function server_to_media(file: any, blob?: null|Blob, status?: Media.Status) {
    const media: Media.Fat = {
        id: browser.string(file._id),
        data: blob || null,
        status: status || "complete",
        created: browser.date(file.created),
        updated: browser.date(file.updated),
        mimetype: browser.string(file.mimetype),
        size: browser.number(file.size),
        filename: browser.string(file.original_filename),
        tags: file.tags.map(browser.string),
    };

    return media;
}

async function upload_media(user: User.Ref, media: Media, token?: Axios.CancelToken) {
    if (!user.id || !media.id || !media.data) { return; }

    // Request authorization to upload the media file
    const request = await api.put(`/user/${user.id}/media/${media.id}/request`, {
        filename: media.filename,
        mimetype: media.mimetype,
        size: media.size,
        tags: media.tags,
    }, { cancelToken: token });

    if (!request || !request.data) { return undefined; }

    // Build form data from the fields received from the server
    const { post: { fields, url: upload_url } } = request.data;
    const form_data = media_form(fields, media.data);

    // Upload the file to s3
    await axios({
        url: upload_url,
        method: "POST",
        data: form_data,
        cancelToken: token,
    });

    // Now we just have to confirm the file upload
    const response = await api.put(`/user/${user.id}/media/${media.id}/confirm`, {
        cancelToken: token,
    });

    if (!response || !response.data) { return undefined; }
    return server_to_media(response.data, media.data);
}

async function fetch_media(ref: Media.Attachment, token?: Axios.CancelToken) {
    const { user: user_id, file: file_id } = ref;

    // Get the information for this media attachment
    const response = await api.get(`/user/${user_id}/media/${file_id}`, {
        cancelToken: token,
    });

    if (!response || !response.data) { return undefined; }

    // Fetch file data for the given image
    const { file, get_url } = response.data;
    const canvas = await urlCanvas(get_url);
    const data_blob = await getCanvasData(canvas);
    return server_to_media(file, data_blob, "complete");
}

async function search_media(user_id: ID, token?: Axios.CancelToken) {
    const response = await api.get(`/user/${user_id}/media`, {
        cancelToken: token,
    });

    if (!response || !response.data) { return []; }
    const { files, pending } = response.data;
    const results: Media.Fat[] = [];

    for (const file of files) {
        results.push(
            server_to_media(file, null, "complete"));
    }

    for (const file of pending) {
        results.push(
            server_to_media(file, null, "pending"));
    }

    return results;
}

async function fetch_profile(user_id?: ID) {
    if (!user_id) { return undefined; }
    return fetch_media({ user: user_id, file: "profile" });
}

async function search_pacs(full_name: string, limit: number, token?: Axios.CancelToken) {
    const response = await api.get(`/pac/?limit=${limit}&full_name=${full_name}`, {
        cancelToken: token,
    });

    if (!response || !response.data) { return []; }

    // Process the pac data from reponse
    const matches: PAC[] = response.data.pacs.map((pac: any) => ({
        id: browser.string(pac._id),
        full_name: browser.string(pac.full_name),
        theme: {
            icon: pac.theme.icon || null,
            primary_color: pac.theme.primary_color || "",
        },
        address: browser.address(pac.addresses[0]),
        phone: browser.phone(pac.phones[0]),
        email: browser.email(pac.emails[0]),
    }));

    return matches;
}

async function fetch_pac(pac_id?: ID, token?: Axios.CancelToken) {
    if (!pac_id) { return undefined; }

    const response = await api.get(`/pac/${pac_id}`, {
        cancelToken: token,
    });

    if (!response || !response.data) { return undefined; }

    const { _id, full_name, theme, addresses, phones, emails } = response.data;
    const pac: PAC = {
        id: browser.string(_id),
        full_name: browser.string(full_name),
        theme: {
            icon: theme.icon || null,
            primary_color: theme.primary_color || "",
        },
        address: browser.address(addresses[0]),
        phone: browser.phone(phones[0]),
        email: browser.email(emails[0]),
    };

    return pac;
}

async function save_pac(user: User.Ref, pac: PAC, token?: Axios.CancelToken) {
    const put_url = pac.id
        ? `/pac/${pac.id}`
        : `/user/${user.id}/pac/${id()}`;

    // Create or update the PAC
    const response = await api.put(put_url, {
        full_name: server.string(pac.full_name),
        addresses: server.address(pac.address),
        phones: server.phone(pac.phone),
        emails: server.email(pac.email),
        theme: {
            icon: pac.theme.icon || null,
            primary_color: pac.theme.primary_color || null,
        },
    }, { cancelToken: token });

    // Unpac response data
    const data = response.data;
    const result: PAC = {
        id: browser.string(data._id),
        full_name: browser.string(data.full_name),
        theme: {
            icon: data.theme.icon || null,
            primary_color: browser.string(data.theme.primary_color),
        },
        address: browser.address(data.addresses[0]),
        phone: browser.phone(data.phones[0]),
        email: browser.email(data.emails[0]),
    };

    return result;
}

async function search_conditions(term: string, limit: number, token?: Axios.CancelToken): Promise<string[]> {
    const response = await api.get(`/patient/conditions?limit=${limit}&query=${term}`, {
        cancelToken: token,
    });

    if (!response || !response.data) { return []; }

    const { conditions } = response.data;
    if (!conditions) { return []; }

    return conditions.map(browser.string);
}

async function register_device(user_id: ID, device: Device, token?: Axios.CancelToken) {
    const device_id = device.id || id();

    delete device.id;
    delete device.created;

    const response = await api.put(`/user/${user_id}/device/${device_id}`, { device }, {
        cancelToken: token,
    });

    return response ? device : undefined;
}

async function unregister_device(user_id: ID, device: Device, token?: Axios.CancelToken) {
    await api.delete(`/user/${user_id}/device/${device.id}`, {
        cancelToken: token,
    });
}

async function search_devices(user_id: ID, token?: Axios.CancelToken) {
    const response = await api.get(`/user/${user_id}/device`, { cancelToken: token });
    if (!response || !response.data) { return []; }

    const devices: any[] = response.data.devices;
    return devices.map(Device.from_server);
}

// All views should call `reset()` on themselves.
// The data that will be passed in is whatever is in the `$reset` property.
Vue.mixin({
    beforeCreate(this: Vue) {
        // Only continue if the Vue provided the $data option
        const { $data } = this.$options as any;
        if (typeof $data !== "object") { return; }

        // Override data function, composing with existing function
        const original = this.$options.data;
        this.$options.data = (...args: any[]) => {
            const orig_data = typeof original === "function"
                ? original.call(this, args)
                : original;

            return Object.assign(orig_data, $data);
        };
    },
});

// Format a date for display
Vue.filter("date", (value: any) => {
    if (!value) { return ""; }
    const date = new Date(value);
    const yyyy = date.getUTCFullYear();
    const MM = ("0" + (date.getUTCMonth() + 1)).slice(-2);
    const dd = ("0" + date.getUTCDate()).slice(-2);
    return `${dd}/${MM}/${yyyy}`;
});

// Format a phone number for display
Vue.filter("phone", (value: string) => {
    if (!value) { return ""; }
    return value.replace(/(\+1)?(\d{3})(\d{3})(\d{3})/, "$1 ($2) $3-$4");
});

/**
 * Simple implementation of a typeahead field.
 * Options and option display are controlled by the parent component.
 */
const SearchField = (() => {

    type Match = any;

    interface Data {
        search: string;
        selection: number;
        is_open: boolean;
        matches: Match[];
    }

    interface Props {
        value: string;
        enabled: boolean;
        placeholder: string;
        options: (search: string) => any;
        formatter: (match: Match) => string;
        select?: (match: Match) => string;
    }

    interface SearchFieldVue extends Props, Data, Vue {
        suggesting: boolean;
        close(): void;
        selector(match: string): string;
        clickWatcher(): void;
    }

    return Vue.extend({
        name: "SearchField",
        template: $("#search-field")[0].outerHTML,
        props: ["value", "options", "select", "formatter", "enabled", "placeholder"],
        data(this: SearchFieldVue) {
            const data: Data = {
                search: this.value,
                selection: -1,
                is_open: false,
                matches: [],
            };

            return data;
        },
        mounted(this: SearchFieldVue) {
            this.clickWatcher();
        },
        computed: {
            suggesting(this: SearchFieldVue) {
                return this.enabled && this.matches.length > 0
                    && (this.is_open || this.search !== "");
            },
        },
        methods: {
            // Close dropdown when user clicks outside of component
            clickWatcher(this: SearchFieldVue) {
                $(document).on("mouseup", (event) => {
                    if (!$.contains(this.$el, event.target)) {
                        this.close();
                    }
                });
            },

            // Determine what value to write into the field
            selector(this: SearchFieldVue, match: string) {
                if (this.select) { return this.select(match); }
                return match;
            },

            // Add the tag at the selected index
            confirm(this: SearchFieldVue, index: number) {
                if (!this.enabled) { return; }

                // Determine what was selected
                const option = this.matches[index] || this.search;

                // Set the search field to the option
                this.search = this.selector(option);
                this.close();

                // Notify of confirmation
                this.$emit("confirm", option);
            },

            // Close the dropdown
            close(this: SearchFieldVue) {
                this.is_open = false;
                this.matches = [];
            },

            // Format the match display
            format(this: SearchFieldVue, match: any) {
                return this.formatter ? this.formatter(match) : match;
            },

            // Cycle through option selection
            // Opens the options even if input is empty
            async up(this: SearchFieldVue) {
                if (!this.enabled) { return; }

                // Set the menu selection
                const selection = this.selection - 1;
                this.selection = selection >= 0 ? selection : this.matches.length - 1;

                // Allow the dropdown to open without a search term
                if (!this.search && !this.is_open) {
                    this.is_open = true;
                    this.matches = await this.options(this.search);
                }
            },

            // Cycle through option selection
            // Opens the options even if input is empty
            async down(this: SearchFieldVue) {
                if (!this.enabled) { return; }

                // Set the menu selection
                const selection = this.selection + 1;
                this.selection = selection % this.matches.length;

                // Allow the dropdown to open without a search term
                if (!this.search && !this.is_open) {
                    this.is_open = true;
                    this.matches = await this.options(this.search);
                }
            },

            // Determine whether the given index is the current selection
            selected(this: SearchFieldVue, index: number) {
                return index === this.selection;
            },
        },
        watch: {
            // Update the value when changed by the parent
            async value(this: SearchFieldVue, newValue: string, oldValue: string) {
                if (newValue === oldValue) { return; }
                if (newValue === this.search) { return; }
                this.search = newValue;
            },

            async search(this: SearchFieldVue, newVal: string, oldVal: string) {
                if (newVal === oldVal) { return; }

                // Reset matches whenever value changes
                const matches = this.matches;
                this.matches = [];

                // Notify that the value has changed
                this.$emit("input", newVal);

                // Only search when the text has changed to a new option
                if (newVal && matches.indexOf(newVal) === -1) {
                    this.matches = await this.options(newVal);
                }
            },

            // Reset selection when matches change
            matches(this: SearchFieldVue) {
                this.selection = this.suggesting ? 0 : -1;
            },
        },
    }) as VueStatic<SearchFieldVue>;
})();

/**
 * Simple implementation of a tag input field.
 * Options and option display are controlled by parent component.
 */
const TagInput = (() => {

    interface Data {
        value_i: any[];
        input_i: string;
        placeholder_i: string;
        enabled_i: boolean;
    }

    interface Props<Tag> {
        value: Tag[];
        display?: (tag: Tag) => string;
        search?: Tag[] | ((search: string) => Tag[]);
        formatter?: (tag: Tag) => string;
        approve?: (tag: Tag) => boolean;
    }

    interface TagInputVue extends Data, Props<{}>, Vue {
        display_i(tag: any): string;
        remove_i(this: TagInputVue, _index: number): void;
        search_i(search: string): any;
        formatter_i(tag: string): string;
        approve_i(input: string): boolean;
        select_i(tag: string): string;
    }

    return Vue.extend({
        name: "TagInput",
        template: $("#tag-input")[0].outerHTML,
        props: ["value", "display", "placeholder", "enabled", "search", "formatter", "approve"],
        data(this: TagInputVue) {
            const $props = this.$props;
            const data: Data = {
                input_i: "",
                value_i: $props.value || [],
                placeholder_i: $props.placeholder || "...",
                enabled_i: $props.enabled != null ? $props.enabled : true,
            };

            return data;
        },
        components: {
            "search-field": SearchField,
        },
        methods: {
            // Forward display call to display function
            display_i(this: TagInputVue, tag: string) {
                if (this.display == null) { return tag; }
                return this.display(tag);
            },

            // Forward options call to option funtion
            search_i(this: TagInputVue, search: string) {
                if (!this.enabled_i) { return; }

                // Return the array of options
                if (Array.isArray(this.search)) {
                    return this.search;
                }

                // Determine the array of options by calling the function
                if (typeof this.search === "function") {
                    return this.search(search);
                }

                // Return nothing
                return [];
            },

            // Forward format call to formatter function
            formatter_i(this: TagInputVue, tag: string) {
                if (this.formatter == null) { return tag; }
                return this.formatter(tag);
            },

            // Forward approval call to approve function
            approve_i(this: TagInputVue, input: string) {
                if (this.approve == null) { return !!input; }
                return this.approve(input);
            },

            // Always clear the input on confirmation
            select_i(this: TagInputVue, _input: string) {
                return "";
            },

            // Remove the tag
            remove_i(this: TagInputVue, index: number) {
                if (!this.enabled_i) { return; }
                this.value_i.splice(index, 1);
                this.$emit("input", this.value_i);
            },

            // Add the tag at the selected index
            confirm_i(this: TagInputVue, option: string) {
                if (!this.enabled_i) { return; }
                if (!this.approve_i(option)) { return; }

                // Add the tag to the list
                this.value_i.push(option);
                this.$emit("input", this.value_i);
            },

            // Remove that last tag when backspacing without text in the input
            backspace_i(this: TagInputVue) {
                if (!this.enabled_i) { return; }
                if (this.input_i) { return; }
                this.remove_i(this.value_i.length - 1);
            },
        },
        watch: {
            // Update when model value changes
            value(this: TagInputVue, newVal: any[], oldVal: any[]) {
                if (newVal === oldVal) { return; }
                this.value_i = newVal;
            },
        },
    }) as VueStatic<TagInputVue>;
})();

/**
 * Logs the user into the application
 */
const LoginForm = (() => {

    interface Data {
        username: string;
        password: string;
        next: string;
    }

    interface LoginFormVue extends Data, Vue {
        mapError(this: LoginFormVue, error: Error): string;
        submit(this: LoginFormVue): Promise<void>;
    }

    return Vue.extend({
        name: "LoginForm",
        template: $("#login")[0].outerHTML,
        data(this: LoginFormVue) {
            const data: Data = {
                username: "",
                password: "",
                next: "/",
            };

            return data;
        },
        methods: {
            // Determine the error to display to the user
            mapError(this: LoginFormVue, error: any) {
                const { response: { data: {error: message}}} = error;
                switch (message) {
                    case "NoSuchUser":
                    case "WrongPassword":
                        return "Invalid credentials. Try again.";
                    default:
                        return "An unknown error has ocurred";
                }
            },

            // Submit the form
            async submit(this: LoginFormVue) {
                const { username, password, next } = this;

                try {
                    const auth = { username, password };
                    const response = await api.get(`/user/me/info`, { auth });

                    // Process the successful response
                    const { _id, roles, patient_id, caregiver_id, pac_id } = response.data;
                    State.User.login({
                        id: _id,
                        username,
                        password,
                        roles,
                        patient_id,
                        caregiver_id,
                        pac_id,
                    });

                    // If user is a PAC admin then load their pac data
                    if (State.User.has_role("pac_admin")) {
                        const pac = await fetch_pac(pac_id);
                        if (pac) { State.PAC.load(pac); }
                    }

                    router.navigate(next);
                } catch (error) {
                    const message = this.mapError(error);
                    toast.api_error(message, "Login", error);
                }
            },
        },
    }) as VueStatic<LoginFormVue>;
})();

/**
 * Account setup.
 * Authenticated via JWT.
 */
const SetupAccount = (() => {

    interface Props {
        next?: string;
        user_id: string;
        auth_token: string;
    }

    interface Data {
        new_password: string;
        confirm_password: string;
    }

    interface SetupAccountVue extends Props, Data, Vue {
        username: string;
        disabled(): boolean;
        mapError(error: any): string;
        submit(): Promise<void>;
    }

    return Vue.extend({
        name: "SetupAccount",
        template: $("#setup-user")[0].outerHTML,
        data(this: SetupAccountVue) {
            const data: Data = {
                new_password: "",
                confirm_password: "",
            };

            return data;
        },
        computed: {
            username(this: SetupAccountVue) {
                // Try to parse the token
                const token = parse_jwt(this.auth_token);
                if (!token) { return ""; }

                // Extract the email address from the token
                return token.body.sub || "";
            },
        },
        methods: {
            disabled(this: SetupAccountVue) {
                return !this.user_id
                    || !this.new_password
                    || !this.confirm_password;
            },

            mapError(error: any) {
                const { response: { data: {error: message}}} = error;
                switch (message) {
                case "AccountLocked": return "Account has been locked";
                case "NoSuchUser": return "User not found";
                case "WrongPassword": return "Token has been revoked";
                case "JsonWebTokenError": return "Token is invalid";
                case "TokenExpiredError": return "Token has expired";
                default: return "An unknown error has ocurred";
                }
            },

            async submit(this: SetupAccountVue) {
                const { user_id, auth_token, username, new_password, confirm_password } = this;

                // Ensure that the passwords match
                if (new_password !== confirm_password) {
                    toast.error("Passwords do not match", "Account Setup");
                    this.new_password = "";
                    this.confirm_password = "";
                    return;
                }

                // Ensure that the password is not empty
                if (!new_password || !confirm_password) {
                    toast.error("Password cannot be empty", "Account Setup");
                    this.new_password = "";
                    this.confirm_password = "";
                    return;
                }

                try {
                    // Set the user's password
                    // Authenticate via token
                    await api.post(`/user/${user_id}`,
                        { password: new_password, username },
                        { token: auth_token });

                    toast.success("Password Updated Successfully!", "Account Setup");
                    router.navigate(this.next || "/");
                } catch (error) {
                    const message = this.mapError(error);
                    toast.api_error(message, "Account Setup", error);
                }
            },
        },
    }) as VueStatic<SetupAccountVue>;
})();

/**
 * Simple control to edit a media file.
 */
const EditMedia = (() => {

    interface MediaFile extends Media {
        status: Media.Status;
        $editing: boolean;
    }

    interface Props {
        value?: MediaFile;
        tags: Media.Status[];
        icon(file: MediaFile): string;
        size(file: MediaFile): number;
        date(file: MediaFile): string;
    }

    interface Data {
        media: MediaFile;
    }

    interface EditMediaVue extends Props, Data, Vue {
        tag_options: Media.Status[];
        editing: Dictionary<MediaFile>;
        options: string[];
        confirm(file: MediaFile): void;
        remove(): void;
        confirm_classes(file: MediaFile): string;
        remove_classes(file: MediaFile): string;
        to_media(file?: File): MediaFile;
        watchFileInput(): void;
    }

    return Vue.extend({
        name: "EditMedia",
        template: $("#edit-media")[0].outerHTML,
        props: ["value", "tags", "icon", "size", "date"],
        data(this: EditMediaVue) {
            const media = this.value
                ? JSON.parse(JSON.stringify(this.value))
                : this.to_media();

            const data: Data = { media };
            return data;
        },
        mounted(this: EditMediaVue) {
            this.watchFileInput();
        },
        components: {
            "tag-input": TagInput,
        },
        computed: {
            tag_options(this: EditMediaVue) {
                return pullAll(this.tags, this.media.tags);
            },
        },
        methods: {

            // Limit tags to those in the approved list
            approve_tag(this: EditMediaVue, tag: Media.Status) {
                return this.tag_options.indexOf(tag) !== -1;
            },

            // Vue.js doesn't seem to have bindings for file inputs
            // Observe the native change event and populate our model
            watchFileInput(this: EditMediaVue) {
                $(this.$el).on("change", "input[type=file]", (event) => {
                    const file = (event.target as any).files[0];

                    // Change the file data
                    this.media = {
                        ...this.to_media(file),
                        id: this.media.id,
                        tags: this.media.tags,
                    };
                });
            },

            // Create a NewFile object from a browser file object
            to_media(this: EditMediaVue, file?: File): MediaFile {
                const media: MediaFile = {
                    ...file_to_media(file),
                    status: "new",
                    $editing: true,
                };

                return media;
            },

            // Notify that the record has been confirmed
            confirm(this: EditMediaVue, media: MediaFile) {
                if (!this.media.data && !this.value) { return; }
                this.$emit("confirm", media);

                $(this.$el)
                    .find("input[type=file]")
                    .val("").trigger("change");
            },

            // Notify that the record has been removed
            remove(this: EditMediaVue) {
                this.$emit("remove", this.media);

                $(this.$el)
                    .find("input[type=file]")
                    .val("").trigger("change");
            },

            confirm_classes(this: EditMediaVue, media: MediaFile) {
                return (media.data || this.value) ? "text-success" : "text-muted";
            },

            remove_classes(this: EditMediaVue, _media: MediaFile) {
                return "text-danger";
            },
        },
    }) as VueStatic<EditMediaVue>;
})();

/**
 * Simple control to show a media file.
 */
const ShowMedia = (() => {

    interface MediaFile extends Media {
        status: Media.Status;
        $editing: boolean;
    }

    interface Props {
        value: MediaFile;
        icon(file: MediaFile): string;
        name(file: MediaFile): string;
        size(file: MediaFile): number;
        date(file: MediaFile): string;
    }

    interface Data {
        file: MediaFile;
    }

    interface ShowMediaVue extends Props, Data, Vue {
        tags: Media.Status[];
        edit(file: MediaFile): void;
        remove(file: MediaFile): void;
        to_media(file?: File): MediaFile;
        row_classes(file: MediaFile): string;
        edit_classes(file: MediaFile): string;
        remove_classes(file: MediaFile): string;
    }

    return Vue.extend({
        name: "ShowMedia",
        template: $("#show-media")[0].outerHTML,
        props: ["value", "icon", "name", "size", "date"],
        data(this: ShowMediaVue) {
            const data: Data = {
                file: this.value || this.to_media(),
            };

            return data;
        },
        components: {
            "tag-input": TagInput,
        },
        methods: {
            // Create a NewFile object from a browser file object
            to_media(this: ShowMediaVue, file?: File): MediaFile {
                const media: MediaFile = {
                    ...file_to_media(file),
                    status: "new",
                    $editing: true,
                };

                return media;
            },

            // Notify that the record has been confirmed
            edit(this: ShowMediaVue, file: MediaFile) {
                if (!file.id) { return; }
                this.$emit("edit", file);
            },

            // Notify that the record has been removed
            remove(this: ShowMediaVue, file: MediaFile) {
                if (!file.id) { return; }
                this.$emit("remove", file);
            },

            row_classes(this: ShowMediaVue, file: MediaFile) {
                switch (file.status) {
                case "pending": return "active";
                case "error": return "danger";
                default: return "";
                }
            },

            edit_classes(this: ShowMediaVue, file: MediaFile) {
                return file.status === "complete" ? "text-warning" : "text-muted";
            },

            remove_classes(this: ShowMediaVue, _file: MediaFile) {
                return "text-danger";
            },
        },
    }) as VueStatic<ShowMediaVue>;
})();

/**
 * Manage files in a medialibrary.
 */
const MediaLibrary = (() => {

    interface MediaFile extends Media.Fat {
        $editing: boolean;
    }

    interface Props {
        user_id: string;
        value: Media.Fat[];
    }

    interface Data {
        files: MediaFile[];
        tags: string[];
    }

    interface MediaLibraryVue extends Props, Data, Vue {
        icon(media: MediaFile): string;
        size(media: MediaFile): string;
        name(media: MediaFile): string;
        date(media: MediaFile): string;
        file(media?: Partial<MediaFile>): MediaFile;
        enhance_media(this: MediaLibraryVue, files: Media.Fat[]): MediaFile[];
        cancel(media: MediaFile): void;
        edit(media: MediaFile): void;
        server_to_media(file: any, status: Media.Status): MediaFile;
        openFiles(): void;
        populateList(): Promise<void>;
        can_upload(media: MediaFile): boolean;
        upload(media: MediaFile): Promise<void>;
        update(media: MediaFile): Promise<void>;
        remove(media: MediaFile): Promise<void>;
    }

    return Vue.extend({
        name: "MediaLibrary",
        template: $("#media-library")[0].outerHTML,
        props: ["value", "user_id"],
        components: {
            "edit-media": EditMedia,
            "show-media": ShowMedia,
        },
        data(this: MediaLibraryVue) {
            const data: Data = {
                files: this.enhance_media(this.value),
                tags: [
                    "profile",
                    "discharge",
                ],
            };

            return data;
        },
        computed: {
            tag_options(this: MediaLibraryVue) {
                const file = this.files.find((file) => file.tags.indexOf("profile") >= 0);
                const options = this.tags.slice();

                if (file) {
                    const index = options.indexOf("profile");
                    options.splice(index, 1);
                }

                return options;
            },
        },
        mounted(this: MediaLibraryVue) {
            this.openFiles();
        },
        methods: {
            // Determine the icon for a given file
            icon(this: MediaLibraryVue, file: MediaFile) {
                if (file.status === "pending" || file.status === "error") {
                    return "fa-spinner fa-spin";
                }

                const [type, sub] = file.mimetype.split("/", 2);
                switch (type) {
                    case "text": return "fa-file-text-o";
                    case "image": return "fa-file-image-o";
                    case "video": return "fa-file-video-o";
                    case "audio": return "fa-file-audio-o";
                    case "application":
                        switch (sub) {
                            case "pdf": return "fa-file-pdf-o";
                            case "zip": return "fa-file-zip-o";
                        }
                    /* falls through */
                    default: return "fa-file-o";
                }
            },

            // Humanize the file size calculation
            size(this: MediaLibraryVue, file: MediaFile) {
                if (!file.size) { return ""; }
                return humanize(file.size, true);
            },

            // Determine the name for the file
            name(this: MediaLibraryVue, file: MediaFile) {
                return $("<a></a>")
                    .attr("rel", file.id)
                    .text(file.filename)[0]
                    .outerHTML;
            },

            // Determine the date for the file
            date(this: MediaLibraryVue, file: MediaFile) {
                if (!file.updated) { return ""; }
                return new Date(file.updated).toLocaleString();
            },

            // Add extra fields to media files
            enhance_media(this: MediaLibraryVue, files: Media.Fat[]) {
                return files.map((file) => ({ ...file, $editing: false }));
            },

            // MediaFile factory function
            file(this: MediaLibraryVue, file?: Partial<MediaFile>) {
                const media: MediaFile = {
                    id: (file && file.id) || "",
                    status: (file && file.status) || "new",
                    created: (file && file.created) || "",
                    updated: (file && file.updated) || "",
                    size: (file && file.size) || 0,
                    filename: (file && file.filename) || "",
                    mimetype: (file && file.mimetype) || "",
                    tags: (file && file.tags) || [],
                    data: (file && file.data) || null,
                    $editing: (file && file.$editing) || false,
                };

                return media;
            },

            // Create media file from server file object
            server_to_media(file: any, status: Media.Status): MediaFile {
                const media: MediaFile = {
                    ...server_to_media(file, null),
                    status,
                    $editing: false,
                };

                return media;
            },

            // Cancel a file edit
            cancel(this: MediaLibraryVue, file: MediaFile) {
                file.$editing = false;
            },

            // Start a file edit
            edit(this: MediaLibraryVue, file: MediaFile) {
                file.$editing = true;
            },

            // Handle file open event
            openFiles(this: MediaLibraryVue) {
                $(this.$el).on("click", "a[rel]", async (event) => {
                    const user_id = this.user_id;
                    const file_id = $(event.target).attr("rel");

                    const response = await api.get(`/user/${user_id}/media/${file_id}`);
                    const { get_url } = response.data;
                    window.open(get_url);
                });
            },

            // Detemine whether a file can be uploaded
            can_upload(this: MediaLibraryVue, file: MediaFile): boolean {
                return !!this.user_id
                    && file.data != null
                    && !!file.filename
                    && !!file.size
                    && !!file.mimetype;
            },

            // Initiate file upload
            async upload(this: MediaLibraryVue, media: MediaFile) {
                // Do nothing if we're not allowed to upload the file
                if (!this.can_upload(media)) { return; }

                // Create a copy of the file object without an observer
                const file = this.file({
                    ...media,
                    id: media.id || id(),
                    status: "pending",
                    $editing: false,
                });

                // Add the file to the files list
                // If the file already exists then replace the current reference
                const index = this.files.findIndex((f) => f.id === file.id);
                if (index === -1) {
                    this.files.unshift(file);
                } else {
                    this.files.splice(index, 1, file);
                }

                try {
                    const user = { id: this.user_id, username: "" };
                    const updated = await upload_media(user, file);
                    Object.assign(file, { ...updated, data: null, status: "complete" });
                } catch (error) {
                    file.status = "error";
                    console.error(error);
                }
            },

            // Update the file's metadata
            async update(this: MediaLibraryVue, media: MediaFile) {
                // Find the original file definition for the edited file
                const file = this.files.find((file) => file.id === media.id);
                if (!file) { return; }

                // Go through the uploading flow when the user has changed the file
                if (media.data) {
                    await this.upload(media);
                }
                // Only update file meta data
                else {
                    // Update important properties of the file
                    file.$editing = false;
                    file.status = "pending";

                    try {
                        // Update the file metadata
                        const user_id = this.user_id;
                        const response = await api.post(`/user/${user_id}/media/${file.id}`, {
                            tags: media.tags,
                            filename: media.filename,
                        });

                        // Write the changed file back
                        const { file: updated } = response.data;
                        Object.assign(file, this.server_to_media(updated, "complete"));
                    } catch (error) {
                        file.status = "error";
                        console.error(error);
                    }
                }
            },

            // Remove files
            async remove(this: MediaLibraryVue, media: MediaFile) {
                const user_id = this.user_id;
                const file_id = media.id;

                if (confirm("Are you sure you want to delete this file?")) {
                    await api.delete(`/user/${user_id}/media/${file_id}`);
                    const index = this.files.indexOf(media);
                    this.files.splice(index, 1);
                }
            },
        },
        watch: {
            value(this: MediaLibraryVue, files: Media.Fat[]) {
                this.files = this.enhance_media(files);
            },
        },
    }) as VueStatic<MediaLibraryVue>;
})();

/**
 * Simple control for editing a device.
 */
const EditDevice = (() => {

    interface Props {
        value?: Device.Normal;
    }

    interface Data {
        device: Device.Normal;
    }

    interface EditDeviceVue extends Props, Data, Vue {
        placeholder: string;
        to_device(device?: Partial<Device.Normal>): Device.Normal;
        confirm(device: Device.Normal): void;
        clear(device: Device.Normal): void;
    }

    return Vue.extend({
        name: "EditDevice",
        template: $("#edit-device")[0].outerHTML,
        props: ["value"],
        created(this: EditDeviceVue) {
            this.$watch("device.type", (newType) => {
                this.device = this.to_device({ type: newType });
            });
        },
        data(this: EditDeviceVue) {
            const data: Data = {
                device: this.to_device(this.value),
            };

            return data;
        },
        computed: {
            placeholder(this: EditDeviceVue) {
                const type = this.device.type as Device.Type;

                switch (type) {
                    case "ios":
                    case "android":
                        return "Version";

                    case "email": return "Email Address";
                    case "sms": return "Phone Number";
                    case "pers": return "IMEI";
                    default: return "";
                }
            },
        },
        methods: {
            to_device(this: EditDeviceVue, device?: Partial<Device.Normal>) {
                const normal: Device.Normal = {
                    id: (device && device.id) || "",
                    type: (device && device.type) || "" as any,
                    created: (device && device.created) || browser.date(Date.now()),
                    value: (device && device.value),
                };

                return normal;
            },

            date(this: EditDeviceVue, device: Device.Normal) {
                return new Date(device.created).toLocaleDateString();
            },

            confirm(this: EditDeviceVue, device: Device.Normal) {
                this.$emit("confirm", device);
                this.device = this.to_device();
            },

            remove(this: EditDeviceVue, device: Device.Normal) {
                this.$emit("remove", device);
                this.device = this.to_device();
            },
        },
        watch: {
            value(this: EditDeviceVue, value: any) {
                this.device = this.to_device(value);
            },
        },
    }) as VueStatic<EditDeviceVue>;
})();

/**
 * Simple control for showing a device
 */
const ShowDevice = (() => {

    interface Props {
        value: Device.Normal;
    }

    interface Data {
        device: Device.Normal;
    }

    interface ShowDeviceVue extends Props, Data, Vue {
        type(device: Device.Normal): string;
        attr(device: Device.Normal): string;
        date(device: Device.Normal): string;
        edit(device: Device.Normal): void;
        remove(device: Device.Normal): void;
    }

    return Vue.extend({
        name: "ShowDevice",
        template: $("#show-device")[0].outerHTML,
        props: ["value"],
        data(this: ShowDeviceVue) {
            const data: Data = {
                device: JSON.parse(JSON.stringify(this.value)),
            };

            return data;
        },
        methods: {
            type(this: ShowDeviceVue, device: Device.Normal) {
                const type = device.type as Device.Type;
                switch (type) {
                    case "ios": return "iOS";
                    case "android": return "Android";
                    case "email": return "Email";
                    case "sms": return "SMS";
                    case "pers": return "PERS";
                    default: return exhaustiveCheck(type);
                }
            },

            attr(this: ShowDeviceVue, device: Device.Normal) {
                switch (device.type) {
                    case "sms":
                        return Phone.format(device.value);

                    case "ios":
                    case "android":
                        return Version.stringify(device.value);

                    default: return device.value;
                }
            },

            date(this: ShowDeviceVue, device: Device.Normal) {
                return new Date(device.created).toLocaleDateString();
            },

            edit(this: ShowDeviceVue, device: Device.Normal) {
                this.$emit("edit", device);
            },

            remove(this: ShowDeviceVue, device: Device.Normal) {
                this.$emit("remove", device);
            },
        },
    }) as VueStatic<ShowDeviceVue>;
})();

/**
 * Simple control for managing notification devices
 */
const ManageDevices = (() => {

    interface Normal extends Device.Normal {
        $editing: boolean;
    }

    interface Props {
        user_id: string;
        value: Device[];
    }

    interface Data {
        devices: Normal[];
        editing: Normal;
    }

    interface ManageDevicesVue extends Props, Data, Vue {
        normalize(device: Device): Normal;
        denormalize(normal: Normal): Device;
        clear(device: Device.Normal): void;
        cancel(device: Device.Normal): void;
        edit(device: Device.Normal): void;
        register(device: Device.Normal): Promise<void>;
        remove(device: Device.Normal): Promise<void>;
    }

    return Vue.extend({
        name: "ManageDevices",
        template: $("#manage-devices")[0].outerHTML,
        props: ["value", "user_id"],
        components: {
            "edit-device": EditDevice,
            "show-device": ShowDevice,
        },
        data(this: ManageDevicesVue) {
            const devices = this.value
                ? this.value.map((device) => this.normalize(device))
                : [];

            const data: Data = {
                devices,
                editing: {
                    id: "",
                    type: "",
                    created: "",
                    value: undefined,
                    $editing: true,
                },
            };

            return data;
        },
        methods: {
            denormalize(this: ManageDevicesVue, normal: Normal): Device {
                const device = Device.denormalize(normal);

                if (device.type === "sms") {
                    device.phone_number = Phone.parse(device.phone_number);
                }

                return device;
            },

            normalize(this: ManageDevicesVue, device: Device): Normal {
                const normal = Device.normalize(device);

                if (device.type === "sms") {
                    device.phone_number = Phone.format(device.phone_number);
                }

                return { ...normal, $editing: false };
            },

            cancel(this: ManageDevicesVue, normal: Normal) {
                normal.$editing = false;
            },

            edit(this: ManageDevicesVue, normal: Normal) {
                if (normal.type === "ios" || normal.type === "android") { return; }
                normal.$editing = true;
            },

            async register(this: ManageDevicesVue, normal: Normal) {
                // Add important properties to the device
                normal.id = normal.id || id();
                normal.$editing = false;

                // Register the device
                const device = this.denormalize(normal);
                const updated = this.normalize(device);
                await register_device(this.user_id, device);

                // Add the device to the devicelist
                const index = this.devices.findIndex((d) => d.id === updated.id);
                if (index === -1) { this.devices.unshift(updated); }
                else { this.devices.splice(index, 1, updated); }
            },

            async remove(this: ManageDevicesVue, normal: Normal) {
                if (confirm("Are you sure you want to remove this device?")) {
                    const device = this.denormalize(normal);
                    await unregister_device(this.user_id, device);

                    const index = this.devices.findIndex((d) => device.id === d.id);
                    this.devices.splice(index, 1);
                }
            },
        },
        watch: {
            value(this: ManageDevicesVue, devices: Device[]) {
                this.devices = devices.map((d) => this.normalize(d));
            },
        },
    }) as VueStatic<ManageDevicesVue>;
})();

/**
 * Simple Image Picker control
 * Creates a media object in a format that is uploadable
 */
const ImagePicker = (() => {

    interface Photo extends Media {
        data: null | Blob;
    }

    interface Props {
        value: Photo;
        accept: string;
        height: number;
        width: number;
    }

    interface Data {
        photo: Photo;
        photo_url: string;
        default_url: string;
        accepts: string;
    }

    interface ImagePickerVue extends Props, Data, Vue {
        default_url: string;
        watchFileInput(): void;
        make_photo(file?: File): Photo;
    }

    return Vue.extend({
        name: "ImagePicker",
        template: $("#image-picker")[0].outerHTML,
        props: ["value", "accept", "height", "width"],
        data(this: ImagePickerVue): Data {
            const default_url = `http://via.placeholder.com/${this.width}x${this.height}`;

            const data: Data = {
                photo: this.value || this.make_photo(),
                photo_url: default_url,
                default_url,
                accepts: this.accept || "image/*",
            };

            return data;
        },
        mounted(this: ImagePickerVue) {
            this.watchFileInput();
        },
        computed: {
            div_styles(this: ImagePickerVue) {
                return {
                    width: "100%",
                    maxWidth: `${this.width}px`,
                    minHeight: `${this.height}px`,
                    overflow: "hidden",
                };
            },
            img_styles(this: ImagePickerVue) {
                return {
                    width: "100%",
                    maxWidth: `${this.width}px`,
                    minHeight: `${this.height}px`,
                };
            },
        },
        methods: {
            // Vue.js doesn't seem to have bindings for file inputs
            // Observe the native change event and populate our model
            watchFileInput(this: ImagePickerVue) {
                // Watch changes to the file input element
                $(this.$el).on("change", "input[type=file]", (event) => {
                    const file = (event.target as any).files[0];

                    // Change the file data
                    this.photo = this.make_photo(file);
                    this.$emit("input", this.photo);
                });

                // Watch changes to the file data blob
                this.$watch("photo.data", (data: null | Blob) => {
                    this.photo_url = this.default_url;
                    if (!(data instanceof Blob)) { return; }

                    const reader = new FileReader();
                    reader.addEventListener("load", () => {
                        this.photo_url = reader.result;
                    });
                    reader.readAsDataURL(data);
                });

                // Watch changes to the starting file
                // This allows the parent component to change what is loaded
                this.$watch("value", (start: Photo) => {
                    this.photo = {
                        id: start.id,
                        data: start.data || null,
                        created: start.created,
                        updated: start.updated,
                        size: start.size,
                        filename: start.filename,
                        mimetype: start.mimetype,
                        tags: start.tags,
                    };
                }, { deep: true });
            },

            // Trigger click on image selector
            select_image(this: ImagePickerVue) {
                $(this.$el).find("input[type=file]").click();
            },

            // Create a file object from a browser file
            make_photo(this: ImagePickerVue, file?: File) {
                const photo: Photo = {
                    id: this.photo.id,
                    tags: this.photo.tags,
                    data: file || null,
                    created: "",
                    updated: "",
                    size: (file && file.size) || 0,
                    filename: (file && file.name) || "",
                    mimetype: (file && file.type) || "",
                };

                return photo;
            },
        },
    }) as VueStatic<ImagePickerVue>;
})();

/**
 * Simple patient record editor
 */
const EditPatient = (() => {

    interface Reset {
        user_id?: undefined | ID;
        pac_id?: undefined | ID;
    }

    interface Photo extends Media.Fat {
        $updated: boolean;
    }

    interface Data {
        $pac_search: null | Axios.CancelTokenSource;
        $user_search: null | Axios.CancelTokenSource;
        $condition_search: null | Axios.CancelTokenSource;
        user: User.Ref;
        pac: PAC.Ref;
        photo: Photo;
        patient: Patient;
        medialibrary: Photo[];
        devicelist: Device[];
    }

    interface EditPatientVue extends Data, Vue {
        reset(options: Reset): void;
        initialize(): Promise<void>;
        make_user(options: Partial<User.Ref>): User.Ref;
        make_pac(options: Partial<PAC.Ref>): PAC.Ref;
        make_patient(options: Partial<Patient>): Patient;
        make_photo(options: Partial<Photo>): Photo;
    }

    return Vue.extend({
        name: "EditPatient",
        template: $("#edit-patient")[0].outerHTML,
        data(this: EditPatientVue) {
            const data: Data = {
                $pac_search: null,
                $user_search: null,
                $condition_search: null,
                user: this.make_user({}),
                pac: this.make_pac({}),
                photo: this.make_photo({}),
                patient: this.make_patient({}),
                medialibrary: [],
                devicelist: [],
            };

            return data;
        },
        components: {
            "search-field": SearchField,
            "image-picker": ImagePicker,
            "tag-input": TagInput,
            "media-library": MediaLibrary,
            "device-list": ManageDevices,
        },
        created(this: EditPatientVue) {
            // Reload the form whenever the user id changes
            this.$watch("user.id", (newVal: string, oldVal: string) => {
                if (newVal === oldVal) { return; }
                router.navigate(`/user/${newVal || "new"}/patient`);
            }, { immediate: true });

            // Initialize the form
            this.initialize();
        },
        methods: {

            // Set the initial form values
            reset(this: EditPatientVue, options: Reset): void {
                this.user = this.make_user({ id: options.user_id });
                this.pac = this.make_pac({});
                this.patient = this.make_patient({});
                this.photo = this.make_photo({});
            },

            // Create a patient object from a partial value
            make_user(this: EditPatientVue, options: Partial<User>) {
                const user: User.Ref = {
                    id: options.id || "",
                    username: options.username || "",
                };

                return user;
            },

            // Create a pac object from partial values
            make_pac(this: EditPatientVue, options: Partial<PAC>) {
                const pac: PAC.Ref = {
                    id: options.id || "",
                    full_name: options.full_name || "",
                };

                return pac;
            },

            // Create a patient object from partial values
            make_patient(this: EditPatientVue, options: Partial<Patient>) {
                const patient: Patient = {
                    id: options.id || "",
                    user_id: options.user_id || "",
                    pac_id: options.pac_id || "",
                    full_name: options.full_name || "",
                    preferred_name: options.preferred_name || "",
                    date_of_birth: options.date_of_birth || "",
                    gender: options.gender || "",
                    blood_type: options.blood_type || "",
                    faith: options.faith || "",
                    instructions: options.instructions || "",
                    conditions: options.conditions || [],
                    allergies: options.allergies || [],
                    address: options.address || "",
                    phone: options.phone || "",
                    email: options.email || "",
                };

                return patient;
            },

            // Create photo object from partial values
            make_photo(this: EditPatientVue, options: Partial<Photo>) {
                const photo: Photo = {
                    id: options.id || "",
                    status: options.status || "complete",
                    data: options.data || null,
                    created: options.created || "",
                    updated: options.updated || "",
                    mimetype: options.mimetype || "",
                    size: options.size || 0,
                    filename: options.filename || "",
                    tags: options.tags || [],
                    $updated: options.$updated || false,
                };

                return photo;
            },

            // Get all of the required user data to populate the form
            async initialize(this: EditPatientVue) {
                const user_id = this.user.id;
                if (!user_id) { return; }

                try {
                    // Get user metadata
                    const info = await fetch_user_info(user_id);
                    if (!info) { return; }

                    // Set user data since we have it now
                    this.user = {
                        id: info.id,
                        username: info.username,
                    };

                    // If the patient doesn't have a pac
                    // It defaults to the pac of the currently logged in user.
                    const pac_id = info.pac_id || this.pac.id;
                    const patient_id = info.patient_id;

                    // Load additional patient data
                    const [patient, pac, photo, devices, medias] = await Promise.all([
                        fetch_patient(patient_id),
                        fetch_pac(pac_id),
                        fetch_profile(user_id),
                        search_devices(user_id),
                        search_media(user_id),
                    ]);

                    // Set all of the data objects that we need
                    if (pac) { this.pac = pac; }
                    if (patient) { this.patient = patient; }
                    if (photo) { this.photo = { ...photo, $updated: false }; }
                    if (devices) { this.devicelist = devices; }
                    if (medias) { this.medialibrary = medias.map((media) => ({ ...media, $updated: false })); }

                } catch (error) {
                    console.error(error);
                }
            },

            // # SEARCH PAC

            edit_pac(this: EditPatientVue) {
                if (!State.User.has_role("sv_admin", "sv_super")) { return; }
                this.pac = this.make_pac({});
            },

            // Format display for the pac
            format_pac(this: EditPatientVue, pac: PAC) {
                return pac.full_name;
            },

            // Handle pac selection
            select_pac(this: EditPatientVue, pac: PAC) {
                this.pac = pac;
            },

            // Search pacs by name
            search_pac: debounce(async function(this: EditPatientVue, term: string): Promise<PAC.Ref[]> {
                // Cancel any network request in flight
                if (this.$pac_search) { this.$pac_search.cancel(); }

                // We will not perform a search without a term
                if (!term) { return []; }

                // Search for pacs by name
                this.$pac_search = axios.CancelToken.source();
                const pacs = await search_pacs(term, 10, this.$pac_search.token);

                // Only return pac refs instead of full objects
                return pacs.map(({ id, full_name }) => ({ id, full_name }));
            }, 500),

            // # SEARCH USER

            edit_user(this: EditPatientVue) {
                this.user = this.make_user({});
            },

            // Format display for user account
            format_user(this: EditPatientVue, user: User) {
                return user.username;
            },

            // Handle user selection
            select_user(this: EditPatientVue, user: User) {
                this.user = user;
            },

            // Search users by username
            search_user: debounce(async function(this: EditPatientVue, term: string): Promise<User.Ref[]> {
                // Cancel in flight request
                if (this.$user_search) { this.$user_search.cancel(); }

                // Do not search without a search term
                if (!term) { return []; }

                // Search for users by username
                this.$user_search = axios.CancelToken.source();
                const users = await search_users(term, this.$user_search.token);

                // Only return user refs instead of full object
                return users.map(({ id, username }) => ({ id, username }));
            }, 500),

            // # SEARCH CONDITIONS

            // Format display for conditions
            format_condition(this: EditPatientVue, condition: string) {
                return condition;
            },

            // Handle condition selection
            select_condition(this: EditPatientVue, condition: string) {
                if (!condition) { return; }
                this.patient.conditions.push(condition);
            },

            // Approve or deny conditions
            approve_condition(this: EditPatientVue, condition: string) {
                return condition && this.patient.conditions.indexOf(condition) === -1;
            },

            // Search for conditions from the database
            search_conditions: debounce(async function(this: EditPatientVue, term: string) {
                // Cancel in-flight network request
                if (this.$condition_search) { this.$condition_search.cancel(); }

                // Search for conditions by name
                this.$condition_search = axios.CancelToken.source();
                const conditions = await search_conditions(term, 10, this.$condition_search.token);

                // Remove any conditions that are already applied
                return pullAll(conditions, this.patient.conditions);
            }, 250),

            // # UPLOAD PHOTO

            // Update the photo object when selection changes
            select_photo(this: EditPatientVue, file: Photo) {
                this.photo = this.make_photo({
                    data: file.data,
                    tags: this.photo.tags,
                    mimetype: file.mimetype,
                    size: file.size,
                    filename: file.filename,
                    $updated: true,
                });
            },

            // # SUBMIT

            can_invite(this: EditPatientVue) {
                return State.User.has_role("pac_admin", "sv_admin", "sv_super");
            },

            // Send the user an invitation to activate their account
            async invite(this: EditPatientVue, patient: Patient) {
                try {
                    await api.put(`/user/${patient.user_id}/forgot`);
                    toast.success("Invitation Sent!", "Manage Patient");
                } catch (error) {
                    toast.api_error("Could not send invitation email", "Error!", error);
                    throw error;
                }
            },

            // Submit the user form
            async submit(this: EditPatientVue, patient: Patient) {
                let user = this.user;

                // Create a new user account
                if (!user.id) {
                    user = await save_user({ ...user, roles: [] });
                }

                // Save the patient object
                this.patient.pac_id = this.pac.id;
                this.patient = await save_patient(user, patient);

                // Upload the profile photo
                if (this.photo.$updated) {
                    this.photo.id = this.photo.id || id();
                    if (this.photo.tags.indexOf("profile") === -1) {
                        this.photo.tags.push("profile");
                    }

                    const media = await upload_media(user, this.photo);
                    if (media) { this.photo = { ...media, $updated: false }; }
                }

                // Set the user object
                // Reloads form on create
                this.user = user;
            },

            disabled() { return false; },
        },
    }) as VueStatic<EditPatientVue>;
})();

/**
 * Create/Edit PAC record
 * PAC record requires an owner in the form of a User account
 */
const EditPAC = (() => {

    interface Color {
        name: string;
        hexCode: string;
    }

    interface Reset {
        user_id?: undefined | ID;
    }

    interface Photo extends Media.Fat {
        $updated: boolean;
    }

    interface Data {
        $user_search: null | Axios.CancelTokenSource;
        user: User.Ref;
        pac: PAC;
        photo: Photo;
        icon: Photo;
        colors: Color[];
    }

    interface EditPACVue extends Data, Vue {
        reset(options: Reset): void;
        initialize(): Promise<void>;
        make_user(options: Partial<User.Ref>): User.Ref;
        make_pac(options: Partial<PAC>): PAC;
        make_photo(options: Partial<Photo>): Photo;
        select_photo(file: Photo): void;
        upload_file(user: User.Ref, photo: Photo): Promise<Photo>;
        upload_profile(user: User.Ref, photo: Photo): Promise<Photo>;
        format_user(user: User): string;
        select_user(user: User): void;
        search_user(term: string): Promise<User.Ref[]>;
    }

    return Vue.extend({
        name: "EditPAC",
        template: $("#edit-pac")[0].outerHTML,
        data(this: EditPACVue) {
            const data: Data = {
                $user_search: null,
                user: this.make_user({}),
                pac: this.make_pac({}),
                photo: this.make_photo({}),
                icon: this.make_photo({}),
                colors: [
                    { name: "None", hexCode: "" },
                    { name: "Turquoise", hexCode: "#40E0D0" },
                    { name: "Steel Blue", hexCode: "#4682B4" },
                    { name: "Cadet Blue", hexCode: "#5F9EA0" },
                    { name: "dodger Blue", hexCode: "#1E90FF" },
                    { name: "Sky Blue", hexCode: "#87CEEB" },
                    { name: "Medium Blue", hexCode: "#0000CD" },
                    { name: "Teal", hexCode: "#008080" },
                    { name: "Yellow Green", hexCode: "#9ACD32" },
                    { name: "Olive Drab", hexCode: "#6B8E23" },
                    { name: "Green", hexCode: "#008000" },
                    { name: "Medium Sea Green", hexCode: "#3CB371" },
                    { name: "Orange", hexCode: "#FFA500" },
                    { name: "Coral", hexCode: "#FF7F50" },
                    { name: "Dark Golden Rod", hexCode: "#B8860B" },
                    { name: "Golden Rod", hexCode: "#DAA520" },
                    { name: "Crimson", hexCode: "#DC143C" },
                    { name: "Medium Purple", hexCode: "#9370DB" },
                ],
            };

            return data;
        },
        created(this: EditPACVue) {
            // Reload the form whenever the user id changes
            this.$watch("user.id", (newVal: string, oldVal: string) => {
                if (newVal === oldVal) { return; }
                router.navigate(`/user/${newVal || "new"}/pac`);
            }, { immediate: true });

            // Initialize the form
            this.initialize();
        },
        components: {
            "search-field": SearchField,
            "tag-input": TagInput,
            "image-picker": ImagePicker,
        },
        methods: {
            // Set the initial form values
            reset(this: EditPACVue, options: Reset): void {
                this.user = this.make_user({ id: options.user_id });
                this.pac = this.make_pac({});
                this.photo = this.make_photo({});
            },

            // Get information required to intialize the form
            async initialize(this: EditPACVue) {
                const user_id = this.user.id;
                if (!user_id) { return; }

                try {
                    const info = await fetch_user_info(user_id);
                    if (!info) { return; }

                    // Set user data since we have it now
                    this.user = { id: info.id, username: info.username };

                    // Load additional PAC data
                    const [pac, photo] = await Promise.all([
                        fetch_pac(info.pac_id),
                        fetch_profile(user_id),
                    ]);

                    if (pac) { this.pac = pac; }
                    if (photo) { this.photo = { ...photo, $updated: false }; }

                    // Load icon image data
                    if (this.pac.theme.icon) {
                        const icon = await fetch_media(this.pac.theme.icon);
                        if (!icon) { this.pac.theme.icon = null; }
                        else { this.icon = { ...icon, $updated: false }; }
                    }

                } catch (error) {
                    toast.api_error("Failed to initialize form", "Error!", error);
                }
            },

            // Create a patient object from a partial value
            make_user(this: EditPACVue, options: Partial<User>) {
                const user: User.Ref = {
                    id: options.id || "",
                    username: options.username || "",
                };

                return user;
            },

            // Create a pac object from partial values
            make_pac(this: EditPACVue, options: Partial<PAC>) {
                const pac: PAC = {
                    id: options.id || "",
                    full_name: options.full_name || "",
                    theme: options.theme || { icon: null, primary_color: "" },
                    address: options.address || "",
                    phone: options.phone || "",
                    email: options.email || "",
                };

                return pac;
            },

            // Create photo object from partial values
            make_photo(this: EditPACVue, options: Partial<Photo>) {
                const photo: Photo = {
                    id: options.id || "",
                    status: options.status || "complete",
                    data: options.data || null,
                    created: options.created || "",
                    updated: options.updated || "",
                    mimetype: options.mimetype || "",
                    size: options.size || 0,
                    filename: options.filename || "",
                    tags: options.tags || [],
                    $updated: options.$updated || false,
                };

                return photo;
            },

            // # SEARCH USER

            // Format display for user account
            format_user(this: EditPACVue, user: User) {
                return user.username;
            },

            // Handle user selection
            select_user(this: EditPACVue, user: User) {
                this.user = this.make_user(user);
            },

            // Search users by username
            search_user: debounce(async function(this: EditPACVue, term: string): Promise<User.Ref[]> {
                // Cancel in flight request
                if (this.$user_search) { this.$user_search.cancel(); }

                // Do not search without a search term
                if (!term) { return []; }

                // Search for users by username
                try {
                    this.$user_search = axios.CancelToken.source();
                    const users = await search_users(term, this.$user_search.token);
                    return users.map(({ id, username }) => ({ id, username }));
                } catch (error) {
                    toast.api_error("Unable to retrieve user list", "User Search", error);
                    throw error;
                }
            }, 500),

            // # UPLOAD PHOTO

            // Update the photo object when selection changes
            select_photo(this: EditPACVue, file: Photo) {
                this.photo = {
                    id: this.photo.id,
                    tags: this.photo.tags,
                    data: file.data,
                    status: file.status,
                    created: file.created,
                    updated: file.updated,
                    mimetype: file.mimetype,
                    size: file.size,
                    filename: file.filename,
                    $updated: true,
                };
            },

            // Update the icon object when selection changes
            select_icon(this: EditPACVue, file: Photo) {
                this.icon = {
                    id: this.icon.id,
                    tags: this.icon.tags,
                    data: file.data,
                    status: file.status,
                    created: file.created,
                    updated: file.updated,
                    mimetype: file.mimetype,
                    size: file.size,
                    filename: file.filename,
                    $updated: true,
                };
            },

            // Upload a file to the medialibrary
            async upload_file(this: EditPACVue, user: User.Ref, photo: Photo) {
                if (!user.id || !photo.data) { return; }

                try {
                    photo.id = photo.id || id();
                    return upload_media(user, photo);
                } catch (error) {
                    toast.api_error("Failed to upload file", "Upload", error);
                    throw error;
                }
            },

            // Upload the photo as the user's profile picture
            async upload_profile(this: EditPACVue, user: User.Ref, photo: Photo) {
                // Ensure the file has the profile tag
                if (photo.tags.indexOf("profile") === -1) {
                    photo.tags.push("profile");
                }

                // Allow the server to determine the file id
                return this.upload_file(user, photo);
            },

            // # STYLES

            // Determine the styles for the color swatch
            color_styles(this: EditPACVue, color: Color) {
                const height = "45px";
                const padding = "5px";
                const border = color.hexCode === this.pac.theme.primary_color
                    ? "1px solid #ccc"
                    : "";
                const cursor = "pointer";

                return { height, padding, border, cursor };
            },

            // Select the given color
            select_color(this: EditPACVue, color: Color) {
                this.pac.theme.primary_color = color.hexCode;
            },

            // # SUBMIT

            can_invite(this: EditPACVue) {
                return State.User.has_role("sv_admin", "sv_super");
            },

            async invite(this: EditPACVue, pac: PAC) {
                try {
                    await api.put(`/pac/${pac.id}/invite`);
                    toast.success("Invitation Sent!", "Manage PAC");
                } catch (error) {
                    toast.api_error("Could not send invitation email", "Error!", error);
                    throw error;
                }
            },

            async submit(this: EditPACVue, pac: PAC) {
                let user = this.user;

                // Create a new user account
                if (!user.id) {
                    try {
                        user = await save_user({ ...user, roles: ["pac_admin"] });
                    } catch (error) {
                        toast.api_error("Failed to create user account", "Submit", error);
                        throw error;
                    }
                }

                // Upload the profile photo
                if (this.photo.$updated) {
                    try {
                        const photo = await this.upload_profile(user, this.photo);
                        if (photo) { this.photo = photo; }
                    } catch (error) {
                        toast.error("Failed to upload profile photo", "Upload", error);
                    }
                }

                // Upload theme icon
                if (this.icon.$updated) {
                    try {
                        const icon = await this.upload_file(user, this.icon);
                        if (icon) {
                            this.icon = icon;
                            this.pac.theme.icon = { user: user.id, file: icon.id };
                        }
                    } catch (error) {
                        toast.api_error("Failed to upload logo", "Upload", error);
                    }
                }

                // Save the PAC object
                try {
                    this.pac = await save_pac(user, pac);
                    this.user = user; // Reloads the form on create
                } catch (error) {
                    toast.error("Failed to save PAC details", "Submit", error);
                    throw error;
                }
            },
        },
    }) as VueStatic<EditPACVue>;
})();

/**
 * Create/Edit CareGiver Record
 */
const EditCareGiver = (() => {

    interface Photo extends Media.Fat {
        $updated: boolean;
    }

    interface Data {
        user: User.Ref;
        pac: PAC;
        caregiver: CareGiver;
        photo: Photo;
    }

    interface EditCareGiverVue extends Data, Vue {
        initialize(): Promise<void>;
        make_photo(options: Partial<Photo>): Photo;
    }

    return Vue.extend({
        name: "EditCareGiver",
        template: $("#edit-caregiver")[0].outerHTML,
        components: {
            "image-picker": ImagePicker,
        },
        data(this: EditCareGiverVue) {
            const data: Data = {
                user: make_user({}),
                pac: make_pac({}),
                caregiver: make_caregiver({}),
                photo: this.make_photo({}),
            };

            return data;
        },
        created(this: EditCareGiverVue) {
            this.initialize();
        },
        methods: {
            async initialize(this: EditCareGiverVue) {
                const user_id = this.user.id;
                if (!user_id) { return; }

                try {
                    const info = await fetch_user_info(user_id);
                    if (!info) { return; }

                    const [caregiver, pac, photo] = await Promise.all([
                        fetch_caregiver(info.caregiver_id),
                        fetch_pac(info.pac_id),
                        fetch_profile(user_id),
                    ]);

                    if (pac) { this.pac = pac; }
                    if (caregiver) { this.caregiver = caregiver; }
                    if (photo) { this.photo = { ...photo, $updated: false }; }

                } catch (error) {
                    toast.api_error("Failed to get data", "Error!", error);
                    throw error;
                }
            },

            // Create a photo object from partial data
            make_photo(this: EditCareGiverVue, options: Partial<Photo>) {
                const photo: Photo = {
                    ...make_media(options),
                    status: options.status || "complete",
                    $updated: options.$updated || false,
                };

                return photo;
            },

            // Update the photo object when selection changes
            select_photo(this: EditCareGiverVue, file: Photo) {
                this.photo = {
                    ...file,
                    id: this.photo.id,
                    tags: this.photo.tags,
                    $updated: true,
                };
            },
        },
    }) as VueStatic<EditCareGiverVue>;
})();

/**
 * Create/Edit User Record
 */
const EditUser = (() => {

    interface Data {
        user: User;
    }

    interface EditUserVue extends Data, Vue { }

    return Vue.extend({
        name: "EditUser",
        template: "<div>Not implemented</div>",
        data(this: EditUserVue) {
            const data: Data = {
                user: make_user({}),
            };

            return data;
        },
    }) as VueStatic<EditUserVue>;
})();

const PatientList = (() => {

    interface PatientListVue extends Vue {}

    return Vue.extend({

    }) as VueStatic<PatientListVue>;
})();

/**
 * Simple control for managing the user's profile
 */
const ManageProfile = (() => {

    interface Data {
        pac: PAC;
        patient: Patient;
        caregiver: CareGiver;
        user: User.Info;
    }

    interface ManageProfileVue extends Data, Vue {
        initialize(): Promise<void>;
    }

    return Vue.extend({
        name: "ManageProfile",
        template: $("#manage-profile")[0].outerHTML,
        components: {
            "tag-input": TagInput,
        },
        created(this: ManageProfileVue) {
            this.initialize();
        },
        data(this: ManageProfileVue) {
            const data: Data = {
                pac: make_pac({}),
                patient: make_patient({}),
                caregiver: make_caregiver({}),
                user: make_user_info({}),
            };

            return data;
        },
        methods: {
            async initialize(this: ManageProfileVue) {
                const user_id = this.user.id;
                if (!user_id) { return; }

                try {
                    const [caregiver, pac, patient] = await Promise.all([
                        fetch_caregiver(this.user.caregiver_id),
                        fetch_pac(this.user.pac_id),
                        fetch_patient(this.user.patient_id),
                    ]);

                    if (pac) { this.pac = pac; }
                    if (patient) { this.patient = patient; }
                    if (caregiver) { this.caregiver = caregiver; }

                } catch (error) {
                    toast.api_error("Failed to get data", "Profile", error);
                    throw error;
                }
            },

            // Edit the user profile
            edit_user(this: ManageProfileVue, _user: User) {
                const user_id = this.user.id;
                if (!user_id) { return; }
                router.navigate(`/user/${user_id}`);
            },

            // Determine whether the user can edit caregiver
            can_edit_caregiver(this: ManageProfileVue) {
                return !!this.caregiver.id;
            },

            edit_caregiver(this: ManageProfileVue) {
                const user_id = this.user.id;
                if (!user_id) { return; }
                router.navigate(`/user/${user_id}/caregiver`);
            },

            // Accept an invite
            async accept_invite(this: ManageProfileVue, invite: CareGiver.Invite) {
                console.log("accept:", invite);
            },

            // Reject an invite
            async reject_invite(this: ManageProfileVue, invite: CareGiver.Invite) {
                console.log("reject:", invite);
            },

            // Determine whether this user can edit patient
            can_edit_patient(this: ManageProfileVue) {
                return !!this.patient.id;
            },

            // Edit the patient info
            edit_patient(this: ManageProfileVue) {
                const user_id = this.user.id;
                if (!user_id) { return; }
                router.navigate(`/user/${user_id}/patient`);
            },

            // Determine whether this user can edit pac
            can_edit_pac(this: ManageProfileVue) {
                return State.User.has_role("pac_admin", "sv_admin", "sv_super");
            },

            // Edit the PAC info
            edit_pac(this: ManageProfileVue) {
                const user_id = this.user.id;
                if (!user_id) { return; }
                router.navigate(`/user/${user_id}/pac`);
            },
        },
    }) as VueStatic<ManageProfileVue>;
})();

// Content wrapper
const content = new Vue({
    el: "#wrapper",
    data: state,
    methods: {

        // Determine whether the user can manage their profile
        can_manage_profile() {
            return !!state.user.id;
        },

        can_create_user() {
            return State.User.has_role("sv_admin", "sv_super");
        },

        // Determine whether the user can create a patient
        can_create_patient() {
            return State.User.has_role("pac_admin", "sv_admin", "sv_super");
        },

        can_create_caregiver() {
            return State.User.has_role("pac_admin", "sv_admin", "sv_super");
        },

        // Determine whether the user can create a PAC
        can_create_pac() {
            return State.User.has_role("sv_admin", "sv_super");
        },
    },
});


// Routes
const router = new Navigo(null, true);
const unauthenticated = ["/login", "/logout"];

(router as any).hooks({
    before: (done: any) => {

        // Do nothing if the last matched route was /login
        const route = (router as any).lastRouteResolved();
        if (route && unauthenticated.indexOf(route.url) >= 0) { return done(); }

        // Do nothing if we are already logged in
        const { username, password, id } = state.user;
        if (username && password && id) { return done(); }

        // Do nothing if querystring contains a token
        // We are authorizing the user to proceed without authentication
        const query = qs(route && route.query);
        if (query.token) { return done(); }

        // Prevent the route from loading
        done(false);

        // Navigate the user to the login page
        // Store the route the user intended to go to
        let login_url = "/login";
        if (route && route.url) { login_url += `?next=${route.url}`; }
        router.navigate(login_url);

    },
});

router.notFound(() => {
    router.navigate("/profile");
});

router.on({

    "/login": (_, querystring) => {
        const query = qs(querystring);
        state.route = LoginForm.extend({
            $data: { next: query.next },
        });
    },

    "/logout": () => {
        State.User.logout();
        router.navigate("/");
    },

    "/profile": () => {
        state.route = ManageProfile.extend({
            $data: { user: state.user },
        });
    },

    "/user/:user_id": (params) => {
        const user_id = params.user_id !== "new" ? params.user_id : "";
        state.route = EditUser.extend({
            $data: { user: { id: user_id } },
        });
    },

    "/user/:user_id/setup": (params, querystring) => {
        const query = qs(querystring);
        state.route = SetupAccount.extend({
            $data: {
                user_id: params.user_id,
                auth_token: query.token,
                next: query.next,
            },
        });
    },

    "/user/:user_id/patient": (params) => {
        const user_id = params.user_id !== "new" ? params.user_id : "";
        state.route = EditPatient.extend({
            $data: {
                user: { id: user_id },
                pac: state.pac, // Default to the logged in user's pac
            },
        });
    },

    "/user/:user_id/caregiver": (params) => {
        const user_id = params.user_id !== "new" ? params.user_id : "";
        state.route = EditCareGiver.extend({
            $data: {
                user: { id: user_id },
                pac: state.pac, // Default to logged in user's pac
            },
        });
    },

    "/user/:user_id/pac": (params) => {
        const user_id = params.user_id !== "new" ? params.user_id : "";
        state.route = EditPAC.extend({
            $data: { user: { id: user_id } },
        });
    },

});

router.resolve();


$(document).on("click", "[route]", (e) => {
    const route = $(e.target).attr("route");
    if (!route) { return; }

    e.preventDefault();
    router.navigate(route);
});
